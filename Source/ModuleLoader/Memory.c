#include "ModuleLoader/Memory.h"
#include "ModuleLoader/Terminal.h"
#include "Library/Size.h"
#define SHOW_EFI_MEMORY

void ConvertMemMap();
void GetCR3(uint64 *address);

enum EFI_MEMORY_TYPE{
EfiReservedMemoryType = 0,
EfiLoaderCode,
EfiLoaderData,
EfiBootServicesCode,
EfiBootServicesData,
EfiRuntimeServicesCode,
EfiRuntimeServicesData,
EfiConventionalMemory,
EfiUnusableMemory,
EfiACPIReclaimMemory,
EfiACPIMemoryNVS,
EfiMemoryMappedIO,
EfiMemoryMappedIOPortSpace,
EfiPalCode,
EfiPersistentMemory,
EfiMaxMemoryType
};

#ifdef SHOW_EFI_MEMORY
const char *EFIMemMapText[] = { 
    "ReservedMemory         ", 
    "LoaderCode             ", 
    "LoaderData             ", 
    "BootServicesCode       ", 
    "BootServicesData       ", 
    "RuntimeServicesCode    ", 
    "RuntimeServicesData    ", 
    "ConventionalMemory     ", 
    "UnusableMemory         ", 
    "ACPIReclaimMemory      ", 
    "ACPIMemoryNVS          ", 
    "MemoryMappedIO         ", 
    "MemoryMappedIOPortSpace", 
    "PalCode                ",
    "PersistentMemory       ", 
    "MaxMemory              "
};
#endif

	uint64 *PML4;
	uint64 *PDPT;
	uint64 *PD;
	uint64 *PT;

	uint64 TotalMemory = 0;
	uint64 EfiPages[16];

typedef struct{
	uint8 Type;
	uint8 Privilege;
	uint64 PhyStart;
	uint64 VirtStart;
	uint64 SizeInByte; //not use for now
	uint64 SizeInPage;
} POS_Memory_Descriptor;

POS_Memory_Descriptor *PMD;

void SetupPaging(EFI_MEMORY_DESCRIPTOR *MemMap, uint64 MemMapSize, uint64 DesSize, uint64 MLSize){
	ParseMemMap(MemMap, MemMapSize, DesSize);
	//UEFI has setting up PAE for us, so no need to relocate the page table.
	//TODO: We know the where are the page table is and got the memory map.
	#ifdef SHOW_EFI_MEMORY
	Print("\n\nType                   |Number of Pages        \n");
	Print("----------------------------------------------");
	for(int i = 0; i < 16; i++){
		Print("\n%s| %u", EFIMemMapText[i], EfiPages[i]);
	}
	#endif
	PMD = (POS_Memory_Descriptor *)(ML_ADDRESS + (MLSize * 4096));
	Print("\nPMD: 0x%x\n", (uint64)&PMD);
	ConvertMemMap();
	TotalMemory *= 4096;
	uint64 converted = SizeConvert(TotalMemory, SIZE_BASE_MB);
	Print("\nTotalMemory: %u MB\n", converted);
	Print("MLSize: %d", MLSize);
}

void ParseMemMap(EFI_MEMORY_DESCRIPTOR* MemMap, uint64 MemMapSize, uint64 DesSize){
	EFI_MEMORY_DESCRIPTOR* map = MemMap;
	uint64 max = MemMapSize / DesSize;
	for(uint64 i = 0; i < max; i++){
	EFI_MEMORY_DESCRIPTOR *temp = (EFI_MEMORY_DESCRIPTOR *)(((uint8 *)map) + (i * DesSize));
	if(temp->NumberOfPages != 0){

	#ifdef SHOW_EFI_MEMORY
	Print("[#%d] Type: %s Phy: 0x%x-0x%x Virt: 0x%x Page: %u\n", i, EFIMemMapText[temp->Type], temp->PhysicalStart, ((temp->PhysicalStart) + ((temp->NumberOfPages) * 4096) - 1), temp->VirtualStart, temp->NumberOfPages);
	#endif

	EfiPages[temp->Type] += temp->NumberOfPages;
	TotalMemory += temp->NumberOfPages;
	}
	}
}

void ConvertMemMap(){
	GetCR3(PML4);
	for(int i = 0; i<4;i++){
		Print("PML4[%d] Present: %s\n", i, PML4[i] & 1 ? "true": "false");
	}
	
}

void EnablePaging(){
	uint64 cr = 0;
	asm(".intel_syntax noprefix;" "xor rax, rax;" "mov rax, cr0;" ".att_syntax prefix;" :"=a" (cr));
	cr += 0x80000000;
	asm(".intel_syntax noprefix;" "mov cr0, rax;" ".att_syntax prefix;": :"a" (cr));
}

void DisablePaging(){
	uint64 cr = 0;
	asm(".intel_syntax noprefix;" "xor rax, rax;" "mov rax, cr0;" ".att_syntax prefix;" :"=a" (cr));
	cr = cr & 0x7fffffff;
	asm(".intel_syntax noprefix;" "mov cr0, rax;" ".att_syntax prefix;": :"a" (cr));
}

void FlushCR3(uint64 *address){
	asm(".intel_syntax noprefix;" "mov cr3, rax;" ".att_syntax prefix;" : : "a" (&address));
}
void GetCR3(uint64 *address){
	asm(".intel_syntax noprefix;" "mov rax, cr3;" ".att_syntax prefix;" : : "a" (&address));
}