#ifndef _MODULELOADER_MEMORY_H_
#define _MODULELOADER_MEMORY_H_
#include "Types.h"
#include "ModuleLoader/BootLoad.h"

typedef struct{
	uint16 Offset : 12;
	uint8 PTIndex;
	uint8 PDIndex;
	uint8 PDPIndex;
	uint8 PML4Index;
	uint16 Unused;
} VirtualAddress;

void SetupPaging(EFI_MEMORY_DESCRIPTOR *MemMap, uint64 MemMapSize, uint64 DesSize, uint64 MLPage);
void ParseMemMap(EFI_MEMORY_DESCRIPTOR* MemMap, uint64 MemMapSize, uint64 DesSize);
void EnablePaging();
void DisablePaging();

#endif